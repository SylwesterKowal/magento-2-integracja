<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Integracja\Model;

class UpdateStockBySourceManagement implements \Kowal\Integracja\Api\UpdateStockBySourceManagementInterface
{

    /**
     * @var \Kowal\Integracja\lib\Worker
     */
    protected $worker;


    public function __construct(
        \Kowal\Integracja\lib\Worker $worker
    )
    {
        $this->worker = $worker;
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdateStockBySource($mass)
    {
        return $this->worker->executeBySource(json_decode($mass, true));


    }
}
