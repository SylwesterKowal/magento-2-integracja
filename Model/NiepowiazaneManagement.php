<?php
declare(strict_types=1);

namespace Kowal\Integracja\Model;

use Kowal\Integracja\lib\MagentoService;

class NiepowiazaneManagement implements \Kowal\Integracja\Api\NiepowiazaneManagementInterface
{

    /**
     * @var MagentoService
     */
    protected $magentoService;


    public function __construct(
        MagentoService $magentoService
    )
    {
        $this->magentoService = $magentoService;
    }

    /**
     * @return array|string
     */
    public function getNiepowiazane()
    {

        $niepowiazane__ = [];
        $niepowiazane = $this->magentoService->_getNiepowiazaneProdukty();
        if (is_array($niepowiazane)) {
            foreach ($niepowiazane as $n) {
                $niepowiazane__[$n['id']] = $n['sku'];
            }
        }
        return $niepowiazane__;
    }
}

