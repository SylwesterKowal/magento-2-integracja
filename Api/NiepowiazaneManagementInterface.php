<?php
declare(strict_types=1);

namespace Kowal\Integracja\Api;

interface NiepowiazaneManagementInterface
{

    /**
     * GET for Niepowiazane api
     * @return string
     */
    public function getNiepowiazane();
}

