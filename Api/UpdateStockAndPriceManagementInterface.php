<?php
declare(strict_types=1);

namespace Kowal\Integracja\Api;

interface UpdateStockAndPriceManagementInterface
{

    /**
     * @param string $mass
     * @return mixed
     */
    public function postUpdateStockAndPrice($mass);
}

