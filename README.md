# Mage2 Module Kowal Integracja

    ``kowal/module-integracja``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Mapowanie produktow

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_Integracja`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-integracja`
 - enable the module by running `php bin/magento module:enable Kowal_Integracja`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - Observer
	- catalog_product_delete_after_done > Kowal\Integracja\Observer\Catalog\ProductDeleteAfterDone

 - API Endpoint
	- GET - Kowal\Integracja\Api\NiepowiazaneManagementInterface > Kowal\Integracja\Model\NiepowiazaneManagement

 - API Endpoint
	- POST - Kowal\Integracja\Api\UpdateStockAndPriceManagementInterface > Kowal\Integracja\Model\UpdateStockAndPriceManagement


## Attributes

 - Product - Powiazany (powiazany)

 - Product - Last update (last_update)

