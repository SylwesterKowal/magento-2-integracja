<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 21:50
 */

namespace Kowal\Integracja\lib;


use Magento\Framework\App\ResourceConnection;

class MagentoService
{
    public $store_id = 0;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }


    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }

    /**
     * @param $sku
     * @return mixed
     */
    public function _getNiepowiazaneProdukty()
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT e.entity_id AS id, e.sku AS sku
FROM " . $this->_getTableName('catalog_product_entity') . " e 
LEFT JOIN " . $this->_getTableName('catalog_product_entity_int') . " v1 ON e.entity_id = v1.entity_id AND v1.store_id = ?
AND v1.attribute_id =
  (SELECT attribute_id
   FROM " . $this->_getTableName('eav_attribute') . "
   WHERE attribute_code = ?
     AND entity_type_id =
       (SELECT entity_type_id
        FROM " . $this->_getTableName('eav_entity_type') . "
        WHERE entity_type_code = 'catalog_product'))
WHERE v1.value = ?";
//        $sql = "SELECT id FROM " . $this->_getTableName('kowal_integracjaartpol_artpol') . " WHERE powiazany = ?";
        return $connection->fetchAssoc(
            $sql,
            [
                0,
                'powiazany',
                1
            ]
        );
    }

    // select * from `table` where `yourfield` >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH)
    public function _getProduktToReset($hour, $prefix = "")
    {
        $filter = "";
        if (!empty($prefix)) $filter = " AND e.sku LIKE '{$prefix}_%'";
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT e.entity_id AS id, e.sku AS sku
                FROM " . $this->_getTableName('catalog_product_entity') . " e 
                LEFT JOIN " . $this->_getTableName('catalog_product_entity_datetime') . " v1 ON e.entity_id = v1.entity_id AND v1.store_id = ?
                AND v1.attribute_id =
                  (SELECT attribute_id
                   FROM " . $this->_getTableName('eav_attribute') . "
                   WHERE attribute_code = ?
                     AND entity_type_id =
                       (SELECT entity_type_id
                        FROM " . $this->_getTableName('eav_entity_type') . "
                        WHERE entity_type_code = 'catalog_product'))
                WHERE v1.value < DATE_SUB(CURDATE(), INTERVAL ? HOUR) {$filter}";

        return $connection->fetchAssoc(
            $sql,
            [
                0,
                'last_update',
                $hour
            ]
        );
    }

    //DELETE FROM `kowal_integracjaartpol_artpol` WHERE `kowal_integracjaartpol_artpol`.`artpol_id` = 4671
    public function _deleteProductInfo($symbol)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "DELETE FROM " . $this->_getTableName('kowal_integracjaartpol_artpol') . " WHERE symbol = ?";
        return $connection->query(
            $sql,
            [
                $symbol
            ]
        );
    }

    public function _getIdFromSku($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne(
            $sql,
            [
                $sku
            ]
        );
    }

    public function checkIfSkuExists($sku, $producent = '')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT sku, type_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku IN (?,?)";
        return $connection->fetchRow($sql, [trim($sku), trim($producent) . '_' . trim($sku)]);
    }

    public function checkIfStockExists($stock_id)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT stock_id  FROM " . $this->_getTableName('inventory_stock') . " WHERE stock_id = ?";
        $result = $connection->fetchRow($sql, [$stock_id]);
        return ($result) ? $stock_id : 1;
    }

    /**
     * @param $sku
     * @param $newQty
     */
    public function _updateStocks($sku, $newQty, $backorder = 0)
    {
        $connection = $this->_getConnection('core_write');
        $productId = $this->_getIdFromSku($sku);

        $sql = "UPDATE " . $this->_getTableName('cataloginventory_stock_item') . " csi
	                   SET
					   csi.qty = ?,
					   csi.manage_stock = 1,
					   csi.use_config_manage_stock = 0,
					   csi.is_in_stock = ?
					   WHERE
					   csi.product_id = ?";

        $sql2 = "UPDATE 
					   " . $this->_getTableName('cataloginventory_stock_status') . " css
	                   SET
	                   css.qty = ?,
					   css.stock_status = ?
					   WHERE
					   css.product_id = ?";


        $isInStock = $newQty > 0 ? 1 : $backorder;
        $stockStatus = $newQty > 0 ? 1 : $backorder;

        $connection->query($sql, array($newQty, $isInStock, $productId));
        $connection->query($sql2, array($newQty, $stockStatus, $productId));
    }

    public function _getInventorySourceItemsToRestet($sku_prefix = "", $source = "default")
    {

        if (!empty($sku_prefix)) {
            $filter = " AND ise.sku LIKE '{$sku_prefix}_%'";
        } else {
            $filter = "";
        }
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT ise.sku AS sku 
                FROM " . $this->_getTableName('inventory_source_item') . " ise 
                WHERE ise.source_code = ?  {$filter}";

        return $connection->fetchAssoc(
            $sql,
            [
                $source
            ]
        );

    }

    public function _updateInventory($sku, $newQty, $source, $stock_id = null)
    {
        $connection = $this->_getConnection('core_write');
        if (!empty($source)) {
            $status = ($newQty > 0) ? 1 : 0;

            $sql = "INSERT INTO " . $this->_getTableName('inventory_source_item') . " ( source_code, sku, quantity, status) VALUES ( ?, ?, ?, ?) ON DUPLICATE KEY UPDATE quantity=VALUES(quantity),status=VALUES(status)";
            $connection->query(
                $sql,
                [
                    $source,
                    $sku,
                    $newQty,
                    $status
                ]
            );


        } else {
            $status = ($newQty > 0) ? 1 : 0;
            $sql = "UPDATE " . $this->_getTableName('inventory_source_item') . " SET `quantity` = ?, `status` = ? WHERE " . $this->_getTableName('inventory_source_item') . ".`sku` = ?";
            $connection->query(
                $sql,
                [
                    $newQty,
                    $status,
                    $sku
                ]
            );
        }

        if ($stock_id > 1) {
            $sql = "INSERT INTO " . $this->_getTableName('inventory_stock_' . $stock_id) . " (sku, quantity, is_salable) VALUES ( ?, ?, ?) ON DUPLICATE KEY UPDATE quantity=VALUES(quantity),is_salable=VALUES(is_salable)";
            $connection->query(
                $sql,
                [
                    $sku,
                    $newQty,
                    $status
                ]
            );
        }

    }

    public function _updateInventoryStatusForBackorder($sku, $backorder, $sources)
    {
        $connection = $this->_getConnection('core_write');
        $sources_ = explode(',',$sources);
        if(is_array($sources_)) {
            foreach ($sources_ as $source) {
                if (!empty($source)) {


                    $sql = "INSERT INTO " . $this->_getTableName('inventory_source_item') . " ( source_code, sku, status) VALUES ( ?, ?, ?) ON DUPLICATE KEY UPDATE status=VALUES(status)";
                    $connection->query(
                        $sql,
                        [
                            $source,
                            $sku,
                            $backorder
                        ]
                    );


                } else {

                    $sql = "UPDATE " . $this->_getTableName('inventory_source_item') . " SET  `status` = ? WHERE " . $this->_getTableName('inventory_source_item') . ".`sku` = ?";
                    $connection->query(
                        $sql,
                        [
                            $backorder,
                            $sku
                        ]
                    );
                }
            }
        }

    }

    /**
     * [1=>not visable, 2,=>catalog, 3=>search, 4=>catalog search]
     * @param $sku
     * @param $stock
     * @return void
     */
    public function setProduktVisibility($sku, $stock)
    {
        if ($stock > 0) {
            $value = 4;
        } else {
            $value = 3;
        }

        $this->updateAttrInt($sku, $value, 'visibility');
    }

    /**
     * @param $sku
     * @param $price
     * @param int $storeId
     */
    public function updatePrices($sku, $price, $marza_default = 0, $cost = null)
    {
        $connection = $this->_getConnection('core_write');
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId('cost');

        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_decimal') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $cost
            ]
        );
        $marza_na_produkt = $this->getAttributeValue($entityId, $this->_getAttributeId('marza'), 'marza');
        if ($marza_na_produkt > 0 && $marza_default > 0 && $marza_na_produkt != $marza_default) {
            $price = $price / (((float)$marza_default + 100) / 100); // odejmujemy marże domyślną
            $price = round($price * (((float)$marza_na_produkt + 100) / 100) , 2);
        }
        $attributeId = $this->_getAttributeId('price');
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_decimal') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $price
            ]
        );
    }

    public function updateAttrVarchar($sku, $value, $code)
    {
        $connection = $this->_getConnection('core_write');
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId($code);
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_varchar') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $value
            ]
        );
    }

    public function updateAttrInt($sku, $value, $code)
    {
        $connection = $this->_getConnection('core_write');
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId($code);
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_int') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $value
            ]
        );
    }

    public function updateAttrDate($sku, $delivery_date, $code)
    {
        $connection = $this->_getConnection('core_write');
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId($code);
        $sql = "INSERT INTO " . $this->_getTableName('catalog_product_entity_datetime') . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
        $connection->query(
            $sql,
            [
                $attributeId,
                $this->store_id,
                $entityId,
                $delivery_date
            ]
        );
    }

    /**
     * @param $attributeCode
     * @return mixed
     */
    private function _getAttributeId($attributeCode)
    {
        $connection = $this->_getReadConnection();
        $sql = "SELECT attribute_id FROM " . $this->_getTableName('eav_attribute') . " WHERE entity_type_id = ? AND attribute_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $this->_getEntityTypeId('catalog_product'),
                $attributeCode
            ]
        );
    }

    /**
     * @param $entityTypeCode
     * @return mixed
     */
    private function _getEntityTypeId($entityTypeCode)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_type_id FROM " . $this->_getTableName('eav_entity_type') . " WHERE entity_type_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $entityTypeCode
            ]
        );
    }

    function getAttributeValue($productId, $attributeId, $attributeKey)
    {
        try {
            $attribute_type = $this->_getAttributeType($attributeKey);

            if ($attribute_type == 'static') return false;

            $connection = $this->_getConnection('core_write');

            $sql = "SELECT value FROM " . $this->_getTableName('catalog_product_entity_' . $attribute_type) . " cped
			WHERE  cped.attribute_id = ?
			AND cped.entity_id = ?";
            return $connection->fetchOne($sql, array($attributeId, $productId));

        } catch (Exception $e) {
            return $e->getMessage() . ' ' . $attributeKey;
        }
    }

    function _getAttributeType($attribute_code = 'price')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT backend_type
				FROM " . $this->_getTableName('eav_attribute') . "
			WHERE
				entity_type_id = ?
				AND attribute_code = ?";
        $entity_type_id = $this->_getEntityTypeId('catalog_product');
        return $connection->fetchOne($sql, array($entity_type_id, $attribute_code));
    }
}
