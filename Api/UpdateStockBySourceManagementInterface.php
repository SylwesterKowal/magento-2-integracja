<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Integracja\Api;

interface UpdateStockBySourceManagementInterface
{

    /**
     * POST for UpdateStockBySource api
     * @param string $mass
     * @return mixed
     */
    public function postUpdateStockBySource($mass);
}
