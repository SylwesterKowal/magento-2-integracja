<?php
declare(strict_types=1);

namespace Kowal\Integracja\Observer\Catalog;

class ProductDeleteAfterDone implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        //Your observer code
    }
}

