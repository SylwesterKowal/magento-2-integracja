<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Integracja\Model;

class ResetStockBySupplyerManagement implements \Kowal\Integracja\Api\ResetStockBySupplyerManagementInterface
{
    /**
     * @var \Kowal\Integracja\lib\Worker
     */
    protected $worker;


    public function __construct(
        \Kowal\Integracja\lib\Worker $worker
    )
    {
        $this->worker = $worker;
    }


    /**
     * {@inheritdoc}
     */
    public function postResetStockBySupplyer($mass)
    {
        return $this->worker->resetStocks(json_decode($mass, true));
    }
}
