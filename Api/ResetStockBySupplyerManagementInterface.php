<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Integracja\Api;

interface ResetStockBySupplyerManagementInterface
{

    /**
     * POST for ResetStockBySupplyer api
     * @param string $mass
     * @return string
     */
    public function postResetStockBySupplyer($mass);
}