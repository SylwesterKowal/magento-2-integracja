<?php
declare(strict_types=1);

namespace Kowal\Integracja\Model;

class UpdateStockAndPriceManagement implements \Kowal\Integracja\Api\UpdateStockAndPriceManagementInterface
{
    /**
     * @var \Kowal\Integracja\lib\Worker
     */
    protected $worker;


    public function __construct(
        \Kowal\Integracja\lib\Worker $worker
    )
    {
        $this->worker = $worker;
    }

    /**
     * @param string $mass
     * @return mixed|string
     */
    public function postUpdateStockAndPrice($mass)
    {
        return $this->worker->execute(json_decode($mass, true));
    }
}

