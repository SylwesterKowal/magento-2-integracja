<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 23:55
 */

namespace Kowal\Integracja\lib;


class Worker
{

    /**
     * @var \Kowal\Integracja\lib\MagentoService
     */
    protected $magentoService;


    public function __construct(
        \Kowal\Integracja\lib\MagentoService $magentoService
    )
    {
        $this->magentoService = $magentoService;
    }


    public function execute($data = [])
    {
        try {
//            file_put_contents("__update.txt",print_r($data,true));

            $count = 0;
            $erros = '';
            $warrning = '';
            foreach ($data as $_data) {
                $count++;
                $sku = $_data['sku'];
                $price = $_data['price'];
                $cost = (isset($_data['cost'])) ? $_data['cost'] : 0;
                $qty = $_data['qty'];
                $marza = $_data['marza'];
                $powiazany = $_data['powiazany'];

                if (!$this->magentoService->checkIfSkuExists($sku)) {
                    $warrning .= $count . '. WARNING:: Product  SKU ' . $sku . ' => nie istnieje' . "\n";
                    continue;
                }
                try {
                    $this->magentoService->_updateStocks($sku, $qty);
                    if (isset($_data['donot_update_price']) && $_data['donot_update_price'] == true) {

                    } else {
                        $this->magentoService->updatePrices($sku, $price, $marza, $cost);
                    }
                    $this->magentoService->updateAttrDate($sku, date('Y-m-d H:i:s'), 'last_update');
                    $this->magentoService->updateAttrInt($sku, $powiazany, 'powiazany');
                    $warrning .= $count . '. INFO:: Product  SKU ' . $sku . ' zaktualizowany stan: ' . $qty . "\n";
                } catch (Exception $e) {
                    $erros .= $count . '. ERROR:: While updating  SKU (' . $sku . ') => ' . $e->getMessage() . "\n";
                }

            }
            return $erros . $warrning;
        } catch (Exception $e) {
            return "Błąd " . $e->getTraceAsString() . "\n";
        }
    }

    public function executeBySource($data = [])
    {
        try {

//            $prefix = (isset($data[0]['prefix'])) ? $data[0]['prefix'] : "";
//            // zerujemy produkty, kture nie byly aktualizowane od 2 godzin
//            // $source_code - filtrujemy tylko dla produktów z prefixem
//            $wycofane_produkty = $this->magentoService->_getProduktToReset(2, $prefix);
//            if (is_array($wycofane_produkty)) {
//                foreach ($wycofane_produkty as $item_) {
//                    $this->magentoService->_updateInventory($item_['sku'], 0, null);
//                }
//            }


            $count = 0;
            $erros = '';
            $warrning = '';
            foreach ($data as $_data) {
                $count++;
                $sku = $_data['sku'];
                $price = (isset($_data['price'])) ? $_data['price'] : null;
                $cost = (isset($_data['cost'])) ? $_data['cost'] : 0;
                $backorder = (isset($_data['backorder'])) ? $_data['backorder'] : 0;
                $backorder_sources_status = (isset($_data['backorder_sources_status'])) ? $_data['backorder_sources_status'] : null;
                $qty = $_data['qty'];
                $marza = $_data['marza'];
                $source_code = $_data['source_code'];
                $stock_id = (isset($_data['stock_id'])) ? $this->magentoService->checkIfStockExists($_data['stock_id']) : 1;

                if (!$this->magentoService->checkIfSkuExists($sku)) {
                    $warrning .= $count . '. WARNING:: Product  SKU ' . $sku . ' => nie istnieje' . "\n";
                    continue;
                }
                try {
                    $this->magentoService->_updateStocks($sku, $qty, $backorder);
                    $this->magentoService->_updateInventory($sku, $qty, $source_code, $stock_id);
                   if(!is_null($backorder_sources_status)) $this->magentoService->_updateInventoryStatusForBackorder($sku, $backorder, $backorder_sources_status);

                    if (isset($_data['donot_update_price']) && $_data['donot_update_price'] == true) {

                    } else {
                        $this->magentoService->updatePrices($sku, $price, $marza, $cost);
                    }
                    $this->magentoService->updateAttrDate($sku, date('Y-m-d H:i:s'), 'last_update');

                    $warrning .= $count . '. INFO:: Product  SKU ' . $sku . ' zaktualizowany stan: ' . $qty . "\n";
                } catch (Exception $e) {
                    $erros .= $count . '. ERROR:: While updating  SKU (' . $sku . ') => ' . $e->getMessage() . "\n";
                }

            }


            return $erros . $warrning;
        } catch (Exception $e) {
            return "Błąd " . $e->getTraceAsString() . "\n";
        }
    }

    public function resetStocks($data = [])
    {
        //file_put_contents("__reset.txt",print_r($data,true));
        $sku_prefix = $data['prefix'];
        $source = ($data['source'] == '') ? 'default' : trim($data['source']);
        if ($items = $this->magentoService->_getInventorySourceItemsToRestet($sku_prefix, $source)) {
            if (!is_array($items)) return "Brak rekordów do wyzerowania";

            foreach ($items as $item) {
                try {
                    $sku = $item['sku'];
                    $this->magentoService->_updateInventory($sku, 0, $source);
                    $this->magentoService->_updateStocks($sku, 0);

                } catch (Exception $e) {
                    return '. ERROR:: While reseting  SKU (' . $sku . ') => ' . $e->getMessage() . "\n";
                }
            }
            return "stany wyzrowane dla source:" . $source;
        } else {
            return "Błąd odczytu z tabeli stanów";
        }
    }
}
