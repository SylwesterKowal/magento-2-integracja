<?php
declare(strict_types=1);

namespace Kowal\Integracja\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Kowal\Integracja\Model\NiepowiazaneManagement;

class Test extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    /**
     * @var NiepowiazaneManagement
     */
    protected $niepowiazaneManagement;


    public function __construct(
        NiepowiazaneManagement $niepowiazaneManagement
    )
    {
        $this->niepowiazaneManagement = $niepowiazaneManagement;
        parent::__construct();
    }


    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);
        $output->writeln("Hello " . $name);
        $products = $this->niepowiazaneManagement->getNiepowiazane(null);
        echo print_r($products, true);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_integracja:test");
        $this->setDescription("TEstowanie danych");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}
